import { Component, OnInit } from '@angular/core';
import {Logins, Usuario, Usuario1} from "../../models/dbData";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  dpi   : any;
  password  : any;
  nombre    : any;
  apellido :any;
  cuenta: any;
  saldo: any;
  correo:any;


  constructor(
      private auth: AuthService,
      private router: Router
  ) {
    this.dpi = "";
    this.password = "";
    this.nombre = "";
    this.apellido=""
    this.correo=""
    this.saldo=""
    this.correo=""
    this.cuenta=""
   }

  ngOnInit(): void {
  }

  Create(): void {
    let vals:Usuario1 = {
      Dpi: this.dpi,
      Nombre: this.nombre,
      Contrasenia: this.password,
      Apellido: this.apellido,
      Correo: this.correo,
      NoCuenta: this.cuenta,
      SaldoInicial: this.saldo
    }
	
    this.auth.getUsers().subscribe(
      (res) => {
        if(res.status == 1) {
          let users = res.users;
          for(let i = 0; i < users.length; i++) {
            let user = users[i];
            if(user[1] == this.dpi) {
              alert('Ya existe el usuario: '+ this.dpi)
              return;
            }
          }
          this.auth.Create(vals).subscribe(
            (data) => {
              if(data.status != -1) {
                alert('Usuario Creado');
                this.router.navigate(['']);
              } else {
                alert('No se puede crear el usuario');
                this.dpi = ''
                this.nombre = ''
                this.password = ''
              }
            }, (err) => console.log(err)
          );
        } else {
          alert('Error en el servidor.');
        }
      }, (err) => console.log(err)
    );
  }

}

