import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { data, transacciones } from '../../models/dbData';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
})
export class PerfilComponent implements OnInit, AfterViewInit {

  dpi: any;
  nombre: any;
  apellido: any;
  cuenta: any;
  saldo: any;
  correo: any;
  contra: any;
  transactions: transacciones[];
  transactions1: transacciones[];
  displayedColumns: string[] = ['Transferencia', 'Monto', 'Origen', 'Destino'];
  // @ts-ignore
  dataSource: MatTableDataSource<transacciones>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private auth: AuthService,
    private router: Router
  ) {
    const aux = JSON.parse(sessionStorage.getItem('User'));
    this.auth.getUsers().subscribe((res) => {
      if (res.status === 1) {
        const users = res.users;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < users.length; i++) {
          if (users[i][0] === aux.Dpi) {
            this.dpi = users[i][0];
            this.nombre = users[i][1];
            this.apellido = users[i][2];
            this.cuenta = users[i][3];
            this.correo = users[i][5];
            this.contra = users[i][6];
            const pet: data = { Dpi: this.dpi };
            this.transactions = [];
            this.transactions1 = [];
            this.auth.getBalance(pet).subscribe((res1) => {
              if (res1.status === 1) {
                this.saldo = res1.SaldoInicial;
              }
            });
            this.auth.getTransactions(pet).subscribe((res1) => {
              if (res1.status === 1) {
                const resultT = res1.origin;
                if (resultT != undefined) {
                  // tslint:disable-next-line:prefer-for-of
                  for (let j = 0; j < resultT.length; j++) {
                    // tslint:disable-next-line:prefer-const
                    let transito: transacciones = {
                      Transferencia: resultT[j][0],
                      Monto: resultT[j][1],
                      Origen: resultT[j][2],
                      Destino: resultT[j][3],
                    };
                    this.transactions.push(transito);
                  }
                }
              }
            });
            this.auth.getReport(pet).subscribe((res1) => {
              if (res1.status === 1) {
                const resultT = res1.origin;
                if (resultT != undefined) {
                  // tslint:disable-next-line:prefer-for-of
                  for (let j = 0; j < resultT.length; j++) {
                    // tslint:disable-next-line:prefer-const
                    let transito: transacciones = {
                      Transferencia: resultT[j][0],
                      Monto: resultT[j][1],
                      Origen: resultT[j][2],
                      Destino: resultT[j][3],
                    };
                    this.transactions1.push(transito);
                  }
                  this.dataSource = new MatTableDataSource<transacciones>(
                    this.transactions1
                  );
                  this.dataSource.paginator = this.paginator;
                }
              }
            });
            break;
          }
        }
      } else {
        alert('Error en el servidor');
      }
    });
  }

  // tslint:disable-next-line:typedef
  generatePDF()
  {
    let docDefinition = {
      content: [
        {
          text: 'REPORTE DE TRANSACCIONES',
          fontSize: 16,
          alignment: 'center',
          color: '#002D62',
          fontFamily: 'Ubuntu'
        },
        {
          text: this.nombre + ' ' + this.apellido,
          fontSize: 20,
          bold: true,
          alignment: 'center',
          decoration: 'underline',
          color: '#002D62',
          fontFamily: 'Ubuntu'
        },
        {
          text: 'Información del Cliente',
          style: 'sectionHeader'
        },
        {
          columns: [
            [
              {
                text: `Cliente: ${this.nombre}`,
                bold: true
              },
              { text: `cuenta: ${this.cuenta }`},
              { text: `DPI: ${this.dpi }`},
              { text: `Saldo: Q. ${this.saldo.toFixed(2) }`}
            ],
            [
              {
                text: `Fecha: ${new Date()}`,
                alignment: 'right'
              }
            ]
          ]
        },
        {
          text: 'Lista de Transacciones',
          style: 'sectionHeader'
        },
        {
          table: {
            headerRows: 1,
            widths: ['*', 'auto', 'auto', 'auto'],
            body: [
              [{text: 'Id Transaccion', bold: true}, {text: 'Cuenta Origen', bold: true}, {text: 'Cuenta Destino', bold: true}, {text: 'Monto', bold: true}],
              ...this.transactions1.map(p => ([p.Transferencia,  p.Origen, p.Destino, 'Q. ' + p.Monto.toFixed(2) ])),
              [{text: 'Total', colSpan: 3}, {}, {}, 'Q. ' + this.transactions1.reduce((sum, p) => sum + (p.Monto), 0).toFixed(2)]
            ]
          }
        },
        {
          text: 'Informacion Adicional',
          style: 'sectionHeader'
        },
        {
          columns: [
            [{ qr: `Cliente:  ${this.nombre + ' ' + this.apellido}, Saldo de la Cuenta: Q. ${this.transactions1.reduce((sum, p) => sum + (p.Monto), 0).toFixed(2)}`, fit: '100' }]
          ]
        },
        {
          text: 'Terminos y Condiciones',
          style: 'sectionHeader'
        },
        {
          ul: [
            'Universidad de San Carlos de Guatemala',
            'Este es una reporte sin valor, los estudiantes y la universidad se encuentran absueltos de cualquier penuria legal',
            'Esto es una demo.',
          ],
        }
      ],
      styles: {
        sectionHeader: {
          bold: true,
          decoration: 'underline',
          fontSize: 14,
          fontFamily: 'Ubuntu',
          margin: [0, 15, 0, 15]
        }
      }
    };
    console.log('Readu')
    pdfMake.createPdf(docDefinition).download('Reporte_Transacciones_' + this.nombre + '_' + this.apellido + '.pdf');
  }



  ngOnInit(): void {

    const aux = JSON.parse(sessionStorage.getItem('User'));
    this.auth.getUsers().subscribe((res) => {
      if (res.status === 1) {
        const users = res.users;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < users.length; i++) {
          if (users[i][0] === aux.Dpi) {
            this.dpi = users[i][0];
            this.nombre = users[i][1];
            this.apellido = users[i][2];
            this.cuenta = users[i][3];
            this.correo = users[i][5];
            this.contra = users[i][6];
            const pet: data = { Dpi: this.dpi };
            this.transactions = [];
            this.transactions1 = [];
            this.auth.getBalance(pet).subscribe((res1) => {
              if (res1.status === 1) {
                this.saldo = res1.SaldoInicial;
              }
            });
            this.auth.getTransactions(pet).subscribe((res1) => {
              if (res1.status === 1) {
                const resultT = res.origin;
                if (resultT != undefined) {
                  // tslint:disable-next-line:prefer-for-of
                  for (let j = 0; j < resultT.length; j++) {
                    // tslint:disable-next-line:prefer-const
                    let transito: transacciones = {
                      Transferencia: resultT[j][0],
                      Monto: resultT[j][1],
                      Origen: resultT[j][2],
                      Destino: resultT[j][3],
                    };
                    this.transactions.push(transito);
                  }
                }
              }
            });
            this.auth.getReport(pet).subscribe((res1) => {
              if (res1.status === 1) {
                const resultT = res.origin;
                if (resultT != undefined) {
                  // tslint:disable-next-line:prefer-for-of
                  for (let j = 0; j < resultT.length; j++) {
                    // tslint:disable-next-line:prefer-const
                    let transito: transacciones = {
                      Transferencia: resultT[j][0],
                      Monto: resultT[j][1],
                      Origen: resultT[j][2],
                      Destino: resultT[j][3],
                    };
                    this.transactions1.push(transito);
                  }
                  this.dataSource = new MatTableDataSource<transacciones>(
                    this.transactions1
                  );
                  this.dataSource.paginator = this.paginator;
                }
              }
            });
            break;
          }
        }
      } else {
        alert('Error en el servidor');
      }
    });
  }

  // tslint:disable-next-line:typedef
  ngAfterViewInit() {
  }

  transacciones(): void {
    this.router.navigateByUrl('transferencia');
  }
}
