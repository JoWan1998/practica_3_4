import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostComponent } from './post.component';

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClient} from "@angular/common/http";
import { PostService } from "../../service/post.service";

describe('PostComponent', () => {
  let component: PostComponent;
  let fixture: ComponentFixture<PostComponent>;

  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let service: PostService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule ],
      providers: [PostService ]
    })
    .compileComponents();
    service = TestBed.inject(PostService);
    httpMock = TestBed.get(HttpClientTestingModule);
    httpClient = TestBed.inject(HttpClient);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('ngOnInit()', () => {
    it('ngOnInit test', () => {
      component.ngOnInit();
      expect(component.publications).toBeUndefined();
      expect(component.src).toBeDefined();
      expect(component.content).toBeDefined();
      expect(component.content).toBeInstanceOf(String);
    }); 
  });

  describe('getPosts()', () => {
    it('getPosts test', () => {
      component.getPosts();
      expect(component.postList).toBeDefined();
      expect(component.user).toBeDefined();
    }); 
  });

  /*describe('createPost()', () => {
    it('createPost test', () => {
      component.createPost();
      expect(component.content).toBeDefined();
    }); 
  });*/

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
