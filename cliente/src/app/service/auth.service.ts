import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import {data, Logins, Usuario, Usuario1, Usuario2} from '../models/dbData';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { api } from '../models/dbConn';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private host = 'http://35.222.237.116:5000';
  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': '*/*',
    }),
  };
  constructor(private http: HttpClient) {}

  getUsers(): Observable<any>{
    return this.http
      .get<any>(`${api.API_URI}${api.USERS}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getBalance(peticion: data): Observable<any>{
    return this.http
      .post<any>(`${api.API_URI}${api.BALANCE}`, peticion, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getTransactions(peticion: data): Observable<any>{
    return this.http
      .post<any>(`${api.API_URI}${api.LAST5}`, peticion, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getReport(peticion: data): Observable<any>{
    return this.http
      .post<any>(`${api.API_URI}${api.REPORTE}`, peticion, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  Update(user: Usuario2): Observable<any> {
    return this.http
    .post<any>(`${this.host}${api.UPDATE}`, user, this.httpOptions)
    .pipe(catchError(this.handleError));
  }

  Login(user: Logins): Observable<any> {
    return this.http
      .post<any>(`${this.host}${api.LOGIN}`, user, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  Create(user: Usuario1): Observable<any> {
    return this.http
      .post<any>(`${this.host}${api.REGISTRO}`, user, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  // tslint:disable-next-line:typedef
  handleError(error: HttpErrorResponse) {
    /*if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }*/
    return throwError('Something bad happened; please try again later.');
  }
}
