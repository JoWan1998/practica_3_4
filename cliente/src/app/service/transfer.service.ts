import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { api } from '../models/dbConn';
import { Transferencia } from '../models/dbData';

@Injectable({
  providedIn: 'root',
})
export class TransferService {
  private httpOptions = {
    headers: new HttpHeaders({
      Accept: '*/*',
    }),
  };
  constructor(private http: HttpClient) {}

  pushTransfer(transferencia: Transferencia) {
    console.log(`${api.USER}/transfer`);
    return this.http.post(
      `${api.USER}/transfer`,
      transferencia,
      this.httpOptions
    );
  }
}
