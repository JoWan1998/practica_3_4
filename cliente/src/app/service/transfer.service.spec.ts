import { HttpClient, HttpClientModule } from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { api } from '../models/dbConn';
import { Transferencia } from '../models/dbData';

import { TransferService } from './transfer.service';

describe('TransferService', () => {
  let service: TransferService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TransferService],
    });
    service = TestBed.get(TransferService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('pushTransfer() should call http Post method for the given route', () => {
    // SET TUP DATA
    const data: Transferencia = {
      Origen: 123456789,
      Destino: 987654321,
      Monto: 150.0,
    };
    // RESULT
    const result = { status: 1 };
    // ASSERT
    const req = httpMock.expectOne(
      (req) => req.method == 'POST' && req.url === `${api.USER}/transfer`
    );
    expect(req.request.method).toBe('POST');
    req.flush(result);
    httpMock.verify();
  });
});
