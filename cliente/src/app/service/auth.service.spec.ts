import {inject, TestBed} from '@angular/core/testing';
import { AuthService } from './auth.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient, HttpErrorResponse, HttpEvent, HttpEventType, HttpHeaders} from '@angular/common/http';
import {data, Logins, Usuario1, Usuario2} from '../models/dbData';
import {api} from '../models/dbConn';

// @ts-ignore
describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [AuthService]
    });
    service = TestBed.get(AuthService);
    httpMock = TestBed.get(HttpTestingController);
  });

  // tslint:disable-next-line:only-arrow-functions
  it('Login() - Ejecucion Correcta', function(){

    // Arrange
    // Set Up Data
    const login: Logins = { NoCuenta: '1998', Contrasenia: '2016jW' };
    const result = { status: 1, id: 1, user: 'Jose' };

    // Act
    // @ts-ignore
    service.Login(login).subscribe(
      (user) =>
      {
        // Assert-1
        expect(user).toEqual(result);
      }
    );

    // Assert -2
    const req = httpMock.expectOne(api.API_URI + api.LOGIN);
    expect(req.request.method).toBe('POST');
    req.flush(result);

    // Assert -3
    httpMock.verify();

  });

  it('Login() - Ejecucion No Correcta', () => {

    //Arrange
    //Set Up Data
    const login:Logins = { NoCuenta: '1998', Contrasenia: '2016' }
    const result = { 'status': 0 }

    //Act
    // @ts-ignore
    service.Login(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user).toEqual(result);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI+ api.LOGIN);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });

  it('Register() - Ejecucion Correcta', () => {

    //Arrange
    //Set Up Data
    const login: Usuario1 = { Dpi: 123456, Nombre: "Jose", Apellido: "Wannan", Contrasenia: "2016jW", Correo: "correo@correo.com", NoCuenta: 1998, SaldoInicial: 1}
    const result = { 'status': 1}

    //Act
    // @ts-ignore
    service.Create(login).subscribe(
      (user) =>
      {
        //Assert-1
        expect(user.status).toEqual(1);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.REGISTRO);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });

  it('Register() - Ejecucion No Correcta', () => {

    //Arrange
    //Set Up Data
    const login: Usuario1 = { Dpi: 123456, Nombre: "Jose", Apellido: "Wannan", Contrasenia: "2016jW", Correo: "correo@correo.com", NoCuenta: 1998, SaldoInicial: 1}
    const result = { 'status': 0}

    //Act
    // @ts-ignore
    service.Create(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user.status).toEqual(0);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.REGISTRO);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });

  it('getUser() - Ejecucion Correcta',() =>{
    //Arrange
    //Set Up Data
    //Act
    // @ts-ignore
    service.getUsers().subscribe(
      (user)=>
      {
        //Assert-1
        expect(user).length > 1;
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.USERS);
    expect(req.request.method).toBe('GET');
    //Assert -3
    httpMock.verify();
  });

  it('Update() - Ejecucion Correcta',() =>{
    //Arrange
    //Set Up Data
    const login: Usuario2 = { username: 'Jose1', fullname: 'Jose Orlando Wannan Escobar', photo: 'fotografia', uid: 2}
    const result = { 'status': 1}

    //Act
    // @ts-ignore
    service.Update(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user.status).toEqual(1);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.UPDATE);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();
  });


  it('Update() - Ejecucion No Correcta',() =>{
    //Arrange
    //Set Up Data
    const login: Usuario2 = { };
    const result = { 'status': 0};

    //Act
    // @ts-ignore
    service.Update(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user.status).toEqual(0);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.UPDATE);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();
  });


  it('throws 404 error - Obtener un error', () => {
    // @ts-ignore
    service.handleError({
      headers: undefined,
      message: "",
      name: "HttpErrorResponse",
      ok: false,
      status: 0,
      statusText: "",
      type: undefined,
      url: undefined,
      error:'ErrorEvent'}).subscribe(
      data => fail('Should have failed with 404 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(undefined);
      }
    );

    const req = httpMock.expectNone(api.API_URI + '/Nadita');

  });

  afterEach(()=>{
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Reporter Service() - Ejecucion Correcta', function()
  {
    const pet: data = { Dpi: 123456 };
    service.getReport(pet).subscribe(
      (res) => {
        expect(res.status).toEqual(1);
      }
    );
    const req = httpMock.expectOne(api.API_URI + api.REPORTE);
    expect(req.request.method).toBe('POST');
    //Assert -3
    httpMock.verify();

  });

  it('Reporter Service() - Ejecucion No Correcta', function()
  {
    const pet: data = { Dpi: 1299999 };
    service.getReport(pet).subscribe(
      (res) => {
        expect(res.status).toEqual(1);
      }
    );
    const req = httpMock.expectOne(api.API_URI + api.REPORTE);
    expect(req.request.method).toBe('POST');
    //Assert -3
    httpMock.verify();

  });


});
