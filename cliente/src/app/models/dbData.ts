export interface Usuario {
  ID_Usuario?: number;
  Usuario?: number;
  Nombre?: string;
  Foto?: string;
  Contrasenia?: string;
}

// tslint:disable-next-line:class-name
export interface data {
  // tslint:disable-next-line:ban-types
  Dpi?: number;
}

// tslint:disable-next-line:class-name
export interface transacciones {
  Transferencia?: number;
  Monto?: number;
  Origen?: number;
  Destino?: number;
}

export interface Usuario1 {
  Dpi?: number;
  Nombre?: string;
  Contrasenia?: string;
  Apellido?: string;
  Correo?: string;
  NoCuenta?: number;
  SaldoInicial?: number;
}

export interface Usuario2 {
  username?: string;
  fullname?: string;
  photo?: string;
  uid?: number;
}

export interface Publicacion {
  Contenido?: string;
  Foto?: string;
  ID_Usuario?: number;
  Nombre_foto?: string;
  Fecha?: Date;
  Usuario?: string;
}

export interface Logins {
  NoCuenta?: string;
  Contrasenia?: string;
}

export interface Transferencia {
  Id?: number;
  Monto?: number;
  Origen?: number;
  Destino?: number;
  Fecha?: string;
}
