export class db {
  static user: string = 'root';
  static pass: string = 'root';
  static db: string = 'practica2';
}

export class api {
  static API_URI: string = 'http://35.222.237.116:5000';
  static LOGIN: string = '/user/login';
  static REGISTRO: string = '/user/create';
  static POSTEAR: string = '/post/create';
  static POSTS: string = '/post';
  static UPDATE: string = '/user/update';
  static USERS: string = '/users';

  static USER: string = `${api.API_URI}/user`;
  static BALANCE: string = '/user/balance';
  static LAST5: string = '/user/last5';
  static REPORTE: string = '/user/reports';
}
