import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { TransferenciaComponent } from './components/transferencia/transferencia.component';
import { ModificacionComponent } from './pages/modificacion/modificacion.component';
import { PostComponent } from './pages/post/post.component';
import { RegisterComponent } from './pages/register/register.component';
import { PerfilComponent } from './pages/perfil/perfil.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'Register', component: RegisterComponent },
  { path: 'Perfil', component: PerfilComponent },
  { path: 'Modification', component: ModificacionComponent },
  { path: 'home', component: PostComponent },
  // RUTA TRANSFERENCIA DE PRUEBA, ELIMINAR Y PASAR EL COMPONENTE AL DASHBOARD O PÁGINA DONDE VA
  { path: 'transferencia', component: TransferenciaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
