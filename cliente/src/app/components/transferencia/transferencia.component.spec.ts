import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Transferencia } from 'src/app/models/dbData';
import { servicesVersion } from 'typescript';

import { TransferenciaComponent } from './transferencia.component';
import { TransferService } from '../../service/transfer.service';
import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('TransferenciaComponent', () => {
  let component: TransferenciaComponent;
  let fixture: ComponentFixture<TransferenciaComponent>;

  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let service: TransferService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TransferenciaComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();
    service = TestBed.inject(TransferService);
    httpMock = TestBed.get(HttpClientTestingModule);
    httpClient = TestBed.inject(HttpClient);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // ASSERT
    expect(component).toBeTruthy();
  });

  it('should be expect json status', () => {
    // ARRANGE
    const data: Transferencia = {
      Origen: 123456789,
      Destino: 987654321,
      Monto: 200,
    };
    const result = {
      status: 1,
    };
    service.pushTransfer(data).subscribe((value) => {
      expect(value).toBe(result);
    });
  });
});
