import { Component, OnInit } from '@angular/core';
import { Transferencia } from 'src/app/models/dbData';
import { TransferService } from 'src/app/service/transfer.service';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.css'],
})
export class TransferenciaComponent implements OnInit {
  transferencia: Transferencia = {
    Origen: null,
    Destino: null,
    Monto: null,
  };

  constructor(private transferService: TransferService) {}

  ngOnInit(): void {}

  cancel(): void {
    this.transferencia.Origen = null;
    this.transferencia.Destino = null;
    this.transferencia.Monto = null;
  }

  transfer(): void {
    console.log(this.transferencia);
    this.transferService
      .pushTransfer(this.transferencia)
      .subscribe((res) => console.log(res));
  }
}
