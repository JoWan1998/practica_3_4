db_creds = {
    "user"      : "admin",
    "password"  : "ad1_grupo2",
    "host"      : "database-ad1.c8astjduuulc.us-east-2.rds.amazonaws.com",
    "database"  : "practica3y4"
}

insertScript = "INSERT INTO Usuario(Dpi, Nombre, Apellido, NoCuenta, SaldoInicial, Correo, Contrasenia) VALUES (%s, %s, %s, %s, %s, %s, %s)"
loginScript = "SELECT Dpi FROM Usuario AS u WHERE u.NoCuenta = %s AND u.Contrasenia = %s"
balanceScript = "SELECT SaldoInicial FROM Usuario AS u WHERE u.Dpi = %s"
updateScript = "UPDATE Usuario AS u SET u.SaldoInicial = %s WHERE u.Dpi = %s"
transferScript = "INSERT INTO Transaccion(Monto, Origen, Destino) VALUES (%s, %s, %s)"
reportScript = "SELECT * FROM Transaccion AS t Where t.Origen = %s OR t.Destino = %s"
Last5Script = "SELECT * FROM Transaccion AS t Where t.Origen = %s OR t.Destino = %s ORDER BY t.ID_Transaccion DESC LIMIT 5"
dpiUserScript = "SELECT dpi FROM Usuario WHERE NoCuenta = %s"
