from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_socketio import SocketIO
import mysql.connector
import json
from datetime import datetime

db_creds = {
    "user"      : "b31572d6da2c7e",
    "password"  : "d8290d1b",
    "host"      : "us-cdbr-east-03.cleardb.com",
    "port"      : "3306",
    "database"  : "heroku_28c40c787472cf1"
}

insertScript = "INSERT INTO Usuario(Dpi, Nombre, Apellido, NoCuenta, SaldoInicial, Correo, Contrasenia) VALUES (%s, %s, %s, %s, %s, %s, %s)"
loginScript = "SELECT Dpi FROM Usuario AS u WHERE u.NoCuenta = %s AND u.Contrasenia = %s"
balanceScript = "SELECT SaldoInicial FROM Usuario AS u WHERE u.Dpi = %s"
updateScript = "UPDATE Usuario AS u SET u.SaldoInicial = %s WHERE u.Dpi = %s"
transferScript = "INSERT INTO Transaccion(Monto, Origen, Destino) VALUES (%s, %s, %s)"
reportScript = "SELECT * FROM Transaccion AS t Where t.Origen = %s OR t.Destino = %s"
Last5Script = "SELECT * FROM Transaccion AS t Where t.Origen = %s OR t.Destino = %s ORDER BY t.ID_Transaccion DESC LIMIT 5"
dpiUserScript = "SELECT dpi FROM Usuario WHERE NoCuenta = %s"

app = Flask(__name__)
CORS(app)
socket = SocketIO(app, cors_allowed_origins="*")

@app.route('/', methods = ['GET'])
def init():
    return "Server working correctly."


@app.route('/users', methods= ['GET'])
def getUsers():
    #try:  
    print(request.get_json())
    db = mysql.connector.connect(**db_creds)
    cursor = db.cursor()
    loginScript = "SELECT * FROM Usuario"
    
    cursor.execute(loginScript)
    result = cursor.fetchall()
    db.close()
    return jsonify({
        "status"    : 1,
        "users"     : result
    })
    #except:
    #   return jsonify({"status": 0})


@app.route('/user/create', methods = ['POST'])
def createUser():
    #try:
    data1 = json.dumps(request.get_json())
    data = json.loads(data1)
    print(data)
    dpi = data["Dpi"]
    name = data["Nombre"]
    lastname = data["Apellido"]
    account = data["NoCuenta"]
    balance = data["SaldoInicial"]
    email = data["Correo"]
    password = data["Contrasenia"]
    values = (dpi, name, lastname, account, balance, email, password)
    print(values)
    db = mysql.connector.connect(**db_creds)
    cursor = db.cursor()
    cursor.execute(insertScript, values)
    db.commit()
    cursor.close()
    db.close()
    print(cursor.rowcount, "record inserted.")
    return jsonify({"status": 1})
    #except:
    #    return jsonify({"status": 0})   


@app.route('/user/login', methods = ['POST'])
def loginUser():
    try:  
        data = request.get_json()
        account = data["NoCuenta"]
        password = data["Contrasenia"]
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        cursor.execute(loginScript, (account, password))
        result = cursor.fetchall()[0]  
        db.close()
        return jsonify({
            "status" : 1,
            "Dpi"     : result[0]
        })
    except:
        return jsonify({"status": 0})


@app.route('/user/balance', methods = ['POST'])
def balanceUser():
    try:
        data = request.get_json()
        dpi = data["Dpi"]
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        cursor.execute(balanceScript, (dpi,))
        result = cursor.fetchall()[0]  
        db.close()
        return jsonify({
            "status" : 1,
            "SaldoInicial": result[0]
        })
    except:
        return jsonify({"status": 0})


@app.route('/user/update', methods =['POST'])
def updateUser():
    try:
        data = request.get_json()
        dpi = data["Dpi"]
        balance = data["SaldoInicial"]
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        cursor.execute(updateScript, (balance, dpi))
        db.commit()
        cursor.close()
        db.close()
        return jsonify({"status" : 1})
    except:
        return jsonify({"status": 0})


@app.route('/user/transfer', methods = ['POST'])
def transferUser():
    try:
        data = request.get_json()
        amount = data["Monto"]
        origin = data["Origen"]
        destiny = data["Destino"]
        print(data)
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        
        cursor.execute(dpiUserScript, (origin,)) # [()] | []
        dpiOrigin = cursor.fetchall()[0][0]
        print(dpiOrigin)
        cursor.execute(dpiUserScript, (destiny,))
        dpiDestiny = cursor.fetchall()[0][0]
        print(dpiDestiny)

        cursor.execute(transferScript, (amount, dpiOrigin, dpiDestiny))
        db.commit()
        cursor.close()
        db.close()
        return jsonify({"status" : 1})
    except:
        return jsonify({"status": 0})


@app.route('/user/reports', methods = ['POST'])
def originUser():
    try:
        data = request.get_json()
        dpi = data["Dpi"]
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        cursor.execute(reportScript, (dpi, dpi))
        result = cursor.fetchall()
        db.close()
        return jsonify({
            "status"    : 1,
            "origin" : result
        })
    except:
        return jsonify({"status": 0})
        
@app.route('/user/last5', methods = ['POST'])
def last5():
    try:
        data = request.get_json()
        dpi = data["Dpi"]
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        cursor.execute(Last5Script, (dpi, dpi))
        result = cursor.fetchall()
        db.close()
        return jsonify({
            "status"    : 1,
            "origin" : result
        })
    except Exception as e: 
        print(str(e))
        return jsonify({"status": 0})

if __name__ == "__main__":
    app.run(host = '0.0.0.0', debug = True)
