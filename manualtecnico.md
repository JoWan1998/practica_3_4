# UNIVERSIDAD SAN CARLOS DE GUATEMALA
# FACULTAD DE INGENIERIA
# ANALISIS Y DISEÑO 1
# PRACTICA 2

### Integrantes

- 201020831	Marco Antonio Fidencio Chávez Fuentes
- 201503811	Heidy Lisseth Mendoza Cardona
- 201612331	José Orlando Wannan Escobar
- 201700965	José Carlos I Alonzo Colocho
- 201730555	Francisco Javier Bran Fuentes

# Manual Tecnico

### Lenguaje utilizado:
- FRONT END: Angular-TypeScript
- BACK  END: Flask-Python
- BD: MySQL
	
### Herramienta para pruebas:
  - Jasmine-Karma

## INDICE
1. [Arquitectura](#Arquitectura-del-Software)
2. [Login](#login)
3. [Registro](#registro)
4. [Transferencias](#Transferencias)
5. [Pruebas Unitarias](#pruebas-unitarias)
6. [Code Coverage](#code-coverage)

## Arquitectura del Software

![This is a alt text.](/images/arqui1.jpeg "This is a sample image.")

## Login 

Para el login se utilizó una prueba unitaria de HttpClient para verificar que la tarea se maneje correctamente utilizando Angular TestBed que configura un entorno bajo prueba que utilizar el modulo HttpClientTestModule en lugar del HttpClient module normal. Al finalizar se inyecta una referencia de HttpTestingController que nos permite manejar el comportamiento en el HttpClient que se ha simulado.

### Ejecucion Correcta

```ts
 it('Login() - Ejecucion Correcta', () => {

    // Arrange
    // Set Up Data
    const login: Logins = { NoCuenta: '1998', Contrasenia: '2016jW' };
    const result = { status: 1, id: 1, user: 'Jose' };

    // Act
    service.Login(login).subscribe(
      (user) =>
      {
        // Assert-1
        expect(user).toEqual(result);
      }
    );

    // Assert -2
    const req = httpMock.expectOne(api.API_URI + api.LOGIN);
    expect(req.request.method).toBe('POST');
    req.flush(result);

    // Assert -3
    httpMock.verify();

  });
 ```
  
 ### Ejecucion No Correcta 
 
 ```ts

  it('Login() - Ejecucion No Correcta', () => {

    //Arrange
    //Set Up Data
    const login:Logins = { NoCuenta: '1998', Contrasenia: '2016' }
    const result = { 'status': 0 }

    //Act
    // @ts-ignore
    service.Login(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user).toEqual(result);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI+ api.LOGIN);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });

  
```


## Registro

Para el Registro se utilizó una prueba unitaria de HttpClient para verificar que la tarea se maneje correctamente utilizando Angular TestBed que configura un entorno bajo prueba que utilizar el modulo HttpClientTestModule en lugar del HttpClient module normal. Al finalizar se inyecta una referencia de HttpTestingController que nos permite manejar el comportamiento en el HttpClient que se ha simulado.

### Ejecucion Correcta

```ts
  it('Register() - Ejecucion Correcta', () => {

    //Arrange
    //Set Up Data
    const login: Usuario1 = { Dpi: 123456, Nombre: "Jose", Apellido: "Wannan", Contrasenia: "2016jW", Correo: "correo@correo.com", NoCuenta: 1234, SaldoInicial: 1}
    const result = { 'status': 1}

    //Act
    // @ts-ignore
    service.Create(login).subscribe(
      (user) =>
      {
        //Assert-1
        expect(user.status).toEqual(1);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.REGISTRO);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });
```

### Ejecucion No Correcta
```ts
    it('Register() - Ejecucion No Correcta', () => {

    //Arrange
    //Set Up Data
    const login: Usuario1 = { Dpi: 123456, Nombre: "Jose", Apellido: "Wannan", Contrasenia: "2016jW", Correo: "correo@correo.com", NoCuenta: 1998, SaldoInicial: 1}
    const result = { 'status': 0}

    //Act
    // @ts-ignore
    service.Create(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user.status).toEqual(0);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.REGISTRO);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });
    
```


## Reportes

Se genero una prueba unitaria de HttpClient para verificar que la tarea se maneje correctamente utilizando Angular TestBed que configura un entorno bajo prueba que utilizar el modulo HttpClientTestModule en lugar del HttpClient module normal. Al finalizar se inyecta una referencia de HttpTestingController que nos permite manejar el comportamiento en el HttpClient que se ha simulado.

### Ejecucion Correcta 

```ts
  it('Reporter Service() - Ejecucion Correcta', function()
  {
    const pet: data = { Dpi: 123456 };
    service.getReport(pet).subscribe(
      (res) => {
        expect(res.status).toEqual(1);
      }
    );
    const req = httpMock.expectOne(api.API_URI + api.REPORTE);
    expect(req.request.method).toBe('POST');
    //Assert -3
    httpMock.verify();

  });
```

### Ejecucion No Correcta

```ts
it('Reporter Service() - Ejecucion No Correcta', function()
  {
    const pet: data = { Dpi: 1299999 };
    service.getReport(pet).subscribe(
      (res) => {
        expect(res.status).toEqual(1);
      }
    );
    const req = httpMock.expectOne(api.API_URI + api.REPORTE);
    expect(req.request.method).toBe('POST');
    //Assert -3
    httpMock.verify();

  });
```

## Manejador de Errores

Ademas, se manejo un test unitario para probar la ejecucion de los ErrorHandlers.

```ts
  it('throws 404 error', () => {
    service.handleError({
      headers: undefined,
      message: "",
      name: "HttpErrorResponse",
      ok: false,
      status: 0,
      statusText: "",
      type: undefined,
      url: undefined,
      error:'ErrorEvent'}).subscribe(
      data => fail('Should have failed with 404 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(undefined);
      }
    );

    const req = httpMock.expectNone(api.API_URI + '/Nadita');

  });
```

## Transferencias

```ts
it('should create', () => {
  // ASSERT
  expect(component).toBeTruthy();
});
```

### Ejecucion No Correcta

```ts
it('should be expect json status', () => {
  // ARRANGE
  const data: Transferencia = {
    Origen: 123456789,
    Destino: 987654321,
    Monto: 200,
  };
  const result = {
    status: 1,
  };
  service.pushTransfer(data).subscribe((value) => {
    expect(value).toBe(result);
  });
});
```

Se requeria de una configuración especifica desde los test para poder realizar los test satisfactoriamente.

```ts
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostService],
    });
    service = TestBed.inject(PostService);
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });
```

## Pruebas Unitarias

### Casos de prueba

![This is a alt text.](/images/arqui2.png "This is a sample image.")

![This is a alt text.](/images/arqui3.png "This is a sample image.")

![This is a alt text.](/images/arqui4.png "This is a sample image.")


## Code Coverage

Para definir el umbral de cobertura de codigo es necesario posicionarse en el documento **karma.conf.js**, y agregar la siguiente configuracion:

```ts
coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly' ],
      fixWebpackSourcePaths: true,
      thresholds: {
        statements: 85,
        lines: 85,
        branches: 85,
        functions: 85
      }
    }
```
